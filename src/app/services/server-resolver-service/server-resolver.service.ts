import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Server } from 'src/app/types/server';
import { ServersService } from '../servers-service/servers.service';

@Injectable({
  providedIn: 'root'
})
export class ServerResolverService implements Resolve<Server> {
  constructor(private serversService: ServersService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Server | Observable<Server> | Promise<Server> {
      const id = parseInt(route.params['id']);
      return this.serversService.getServer(id);
  }

}
