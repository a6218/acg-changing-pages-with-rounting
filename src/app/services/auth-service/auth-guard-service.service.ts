import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { map, Observable } from 'rxjs';
import { AuthService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  isLoggedIn(): Promise<boolean> {
      return this.authService.isAuthenticated().then(
        (authentication: boolean) => {
          console.log('authentication: ', authentication);
          if (authentication) {
            return true
          } else {
            this.router.navigate(['/']);
            return false;
          }
        }
      );
  }

  isOLoggedIn(): Observable<boolean> {
    return this.authService
      .isOAuthenticated()
      .pipe(
        map((authentication: boolean) => {
          console.log('authtentication: ', authentication);
          if (authentication) {
            return true
          } else {
            this.router.navigate(['/']);
            return false;
          }
        }),
      );
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {
      // return this.isLoggedIn();
      return this.isOLoggedIn();
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    boolean | Observable<boolean> | Promise<boolean>
  {
    return this.canActivate(childRoute, state);
  }
}
