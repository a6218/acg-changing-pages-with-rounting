import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loggedIn: boolean = false;

  isAuthenticated(): Promise<boolean> {
    const promise = new Promise<boolean>(
      (resolve, reject) => {
        setTimeout(() => {
          resolve(this.loggedIn)
        }, 800);
      }
    );

    return promise;
  }

  isOAuthenticated(): Observable<boolean> {
    const observer = new Observable<boolean>(
      (observer) => {
        setTimeout(() => {
          observer.next(this.loggedIn)
        }, 800);
      }
    );

    return observer;
  }

  login() {
    this.loggedIn = true;
  }

  logout() {
    this.loggedIn = false;
  }
}
