import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { EditServerComponent } from './components/servers/edit-server/edit-server.component';
import { ServerComponent } from './components/servers/server/server.component';
import { ServersComponent } from './components/servers/servers.component';
import { UserComponent } from './components/users/user/user.component';
import { UsersComponent } from './components/users/users.component';
import { AuthGuardService } from './services/auth-service/auth-guard-service.service';
import { CanDeactivateGuardService } from './services/can-deactivate-guard-service/can-deactivate-guard.service';
import { ServerResolverService } from './services/server-resolver-service/server-resolver.service';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
  },
  {
    path: 'servers',
    // canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    component: ServersComponent,
    children: [
      {
        path: ':id', component: ServerComponent,
        resolve: { server: ServerResolverService },
      },
      {
        path: ':id/edit',
        component: EditServerComponent,
        canDeactivate: [CanDeactivateGuardService],
      },
    ],
  },
  {
    path: 'users', component: UsersComponent, children: [
      { path: ':id/:name', component: UserComponent, },
    ],
  },
  {
    // path: 'not-found', component: PageNotFoundComponent,
    path: 'not-found', component: ErrorPageComponent,
    data: { message: 'Page not Found' },
  },
  // HAS TO BE the LAST ENTRY for error handling
  {
    path: '**', redirectTo: '/not-found',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
