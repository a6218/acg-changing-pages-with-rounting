import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CanComponentDeactivate } from 'src/app/services/can-deactivate-guard-service/can-deactivate-guard.service';
import { ServersService } from 'src/app/services/servers-service/servers.service';
import { Server } from 'src/app/types/server';

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.scss']
})
export class EditServerComponent implements OnInit, CanComponentDeactivate {
  server!: Server | undefined;
  serverName: string = '';
  serverStatus: string = '';
  serverId: number = 1;
  allowEdit: boolean = false;
  changesSaved: boolean = false;

  constructor(
    private serversService: ServersService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.server = this.serversService.getServer(this.serverId);
    this.serverName = this.server!.name;
    this.serverStatus = this.server!.status;

    this.route.params
      .subscribe(
        (params: Params) => {
          if (params['id'] === undefined) {
            return;
          }

          this.serverId = parseInt(params['id']);
          this.server = this.serversService.getServer(this.serverId);
          this.serverName = this.server!.name;
          this.serverStatus = this.server!.status;
        }
      );

    this.route.queryParams
        .subscribe(
          (queryParams: Params) => {
            if (queryParams['allowEdit'] === undefined) {
              return;
            }

            this.allowEdit = false;
            if (queryParams['allowEdit'] === '1') {
              this.allowEdit = true;
            }
          }
        );
  }

  onUpdateServer() {
    this.serversService.updateServer(
      this.server!.id,
      {name: this.serverName, status: this.serverStatus}
    );
    this.changesSaved = true;
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  canDeactivate(
  ):
    Observable<boolean> | Promise<boolean> | boolean
  {
    if (this.allowEdit === false) {
      return true;
    }

    if (
      (
        this.serverName !== this.server!.name
        || this.serverStatus !== this.server!.status
      )
      && this.changesSaved === false
    ) {
      return confirm('Do you want to discard changes?');
    }
    return true;
  };
}
